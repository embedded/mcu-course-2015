############
# Settings #
############

PROJECT		= $(shell basename `pwd`)
MCU			= cortex-m4
MFLOAT		= hard
MFPU		= fpv4-sp-d16
HNAME		= STM32F411xE
OPTIM_LEVEL	= g
DEBUG_LEVEL	= 3

#########
# Paths #
#########

GCC_BASE = /usr/local/gcc-arm-none-eabi
GCC_LIB	 = $(GCC_BASE)/arm-none-eabi/lib/
GCC_INC	 = $(GCC_BASE)/arm-none-eabi/include/
CROSSC	 = $(GCC_BASE)/bin/arm-none-eabi-
AS		 = $(CROSSC)as
CC		 = $(CROSSC)gcc
CPP		 = $(CROSSC)g++
LD		 = $(CROSSC)gcc
OBJCOPY	 = $(CROSSC)objcopy
OBJDUMP	 = $(CROSSC)objdump
SIZE	 = $(CROSSC)size

#################
# More Settings #
#################

# Objects
PROJECT_SOURCE    = main.c
PROJECT_OBJECTS	  = 
PROJECT_SYMBOLS	  =

# Includes
LINKER_SCRIPT = -T mem.ld -T libs.ld -T sections.ld
INCLUDE_PATHS = . $(GCC_INC) \
    "include" \
    "include/cmsis" \
    "include/hal/inc" \
	"include/hal/src" \
    "include/math" \
    "include/stm32f4xx"

LIBRARY_PATHS = . $(GCC_LIB)armv7e-m/fpu ldscripts

# Create flags from paths
INCLUDES = $(addprefix -I,$(INCLUDE_PATHS))
LIBRARY  = $(addprefix -L,$(LIBRARY_PATHS))

# Size optimisation
ifeq (OPTIM_LEVEL, s)
  LIBRARIES = -lstdc++_s -lsupc++_s -lm -lc_s -lg_s -lnosys
  SYS_LD_FLAGS  = --specs=nano.specs -u _printf_float -u _scanf_float
else
  LIBRARIES = -lstdc++ -lsupc++ -lm -lc -lg -lnosys
  SYS_LD_FLAGS  =
endif

# For debugging
ifdef DEBUG_LEVEL
  CC_DEBUG_FLAGS = -g$(DEBUG_LEVEL)
  CC_SYMBOLS = -DDEBUG $(PROJECT_SYMBOLS)
else
  CC_DEBUG_FLAGS =
  CC_SYMBOLS = -DNODEBUG $(PROJECT_SYMBOLS)
endif

# Compiler flags
MCU_CC_FLAGS = -mthumb -mcpu=$(MCU) -mfloat-abi=$(MFLOAT) -mfpu=$(MFPU) -D$(HNAME)
CC_FLAGS = $(MCU_CC_FLAGS) -c -O$(OPTIM_LEVEL) $(CC_DEBUG_FLAGS) -fno-common -fmessage-length=0 -Wall -fno-exceptions -ffunction-sections -fdata-sections
LD_FLAGS = $(MCU_CC_FLAGS) -Wl,--gc-sections $(SYS_LD_FLAGS) -Wl,-Map=$(PROJECT).map

################
# Object files #
################
# Filter for C++
CPPFILES = $(filter %.cpp, $(PROJECT_SOURCE))
CCFILES = $(filter %.cc, $(PROJECT_SOURCE))
BIGCFILES = $(filter %.C, $(PROJECT_SOURCE))

# Filter for C
CFILES = $(filter %.c, $(PROJECT_SOURCE))

# Filter for Assembly
ASMFILES = $(filter %.S, $(PROJECT_SOURCE))

# Create a list of other includes
PROJECT_SOURCES = $(shell find include -name "*.c")

PROJECT_OBJECTS = $(PROJECT_SOURCES:.c=.o) $(CFILES:.c=.o) $(CPPFILES:.cpp=.o) $(BIGCFILES:.C=.o) $(CCFILES:.cc=.o) $(ASMFILES:.S=.o)

######################
# Makefile functions #
######################
.SUFFIXES : .c .cc .cpp .C .o .out .s .S .hex .ee.hex .h .hh .hpp 
.PHONY: writeflash clean stats size

all: $(PROJECT).bin

stats: $(PROJECT).bin
	$(OBJDUMP) -h $(PROJECT).elf
	$(SIZE) $(PROJECT).elf

size: $(PROJECT).bin
	$(SIZE) $(PROJECT).elf

writeflash: $(PROJECT).bin
	st-flash write $(PROJECT).bin 0x8000000

install: writeflash

clean:
	rm -f $(PROJECT).bin $(PROJECT).elf $(PROJECT_OBJECTS)

.s.o:
	$(AS) $(MCU_CC_FLAGS) -o $@ $<

.c.o:
	$(CC)  $(CC_FLAGS) $(CC_SYMBOLS) -std=gnu99	  $(INCLUDES) -o $@ $<

.cpp.o:
	$(CPP) $(CC_FLAGS) $(CC_SYMBOLS) -std=gnu++98 $(INCLUDES) -o $@ $<

$(PROJECT).elf: $(PROJECT_OBJECTS)
	$(LD) $(LD_FLAGS) $(LINKER_SCRIPT) $(LIBRARY) -o $@ $^ $(LIBRARIES)

$(PROJECT).bin: $(PROJECT).elf
	$(OBJCOPY) -S -O binary $< $@

