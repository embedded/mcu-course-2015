/*
 * Author: Silke Hofstra
 * Date: 20-05-15
 * Chip: STM32 F411RE
 * Description:
 */

#include <stdio.h>
#include <stdlib.h>

#include "_initialize_hardware.c"
#include "stm32f4xx_nucleo.h"
#include "Timer.c"

int
main(int argc, char* argv[])
{
    // Setup
    BSP_LED_Init(LED2);
    timer_start();
    
    // Infinite loop
    while (1)
    {
        BSP_LED_Toggle(LED2);
        timer_sleep(500);
    }
    // Infinite loop, never return.
}
